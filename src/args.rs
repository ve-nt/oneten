use crate::char_set::CharacterSet;
use crate::universe::Cell;
use structopt::StructOpt;

// Holds all the command-line arguments that can be passed to the
// program. Fields are filled with `Args::get()`.
#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab")]
pub struct Args {
    /// Verbose output
    #[structopt(short, long)]
    pub verbose: bool,

    /// Number of times to iterate the universe.
    #[structopt(short, long, default_value = "100")]
    pub iterations: u32,

    /// Rule to use for iteration.
    #[structopt(short, long, default_value = "110")]
    pub rule: u32,

    /// List available character presets
    #[structopt(short, long)]
    pub list_charsets: bool,

    /// Starting universe comprised as a sequence of 1s and 0s.
    #[structopt(parse(from_str), required_unless = "list-charsets")]
    universe: Option<String>,

    /// Character set used to represent cells.
    #[structopt(short, long, default_value = "odot")]
    char_set: String,

    /// Custom character set
    #[structopt(short = "C", long = "custom-char-set")]
    custom_char_set: Option<String>,
}

impl Args {
    pub fn get() -> Self {
        Self::from_args()
    }

    pub fn universe(&self) -> Vec<Cell> {
        let mut universe = Vec::new();
        for cell in self.universe.clone().unwrap().chars() {
            match cell {
                '0' => universe.push(Cell::Dead),
                '1' => universe.push(Cell::Alive),
                _ => {
                    eprintln!(
                        "{}: Unrecognised character '{}' in universe. Only '1' or '0' is valid.",
                        crate::PKG_NAME,
                        cell
                    );
                    std::process::exit(1);
                }
            }
        }
        universe
    }

    pub fn char_set(&self) -> (char, char) {
        CharacterSet::from_str(&self.char_set.to_lowercase()).chars()
    }

    pub fn custom_char_set(&self) -> Option<(char, char)> {
        match &self.custom_char_set {
            None => None,
            Some(cs) => {
                if cs.chars().count() == 2 {
                    let char_set: Vec<char> = cs.chars().collect();
                    Some((char_set[0], char_set[1]))
                } else {
                    eprintln!(
                        "{}: Incorrect character set format '{}'. \
                         Please provide 2 characters to use for the character set.",
                        crate::PKG_NAME,
                        cs
                    );
                    std::process::exit(1);
                }
            }
        }
    }
}
