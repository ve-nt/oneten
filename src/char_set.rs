use std::fmt;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use CharacterSet::*;

#[derive(Debug, EnumIter)]
pub enum CharacterSet {
    ODot,
    Binary,
    Blocks,
    Hexagons,
    Squares,
    Circles,
    Triangles,
    Parallelograms,
}

impl CharacterSet {
    pub fn from_str(s: &str) -> Self {
        match s {
            "odot" => ODot,
            "binary" => Binary,
            "blocks" => Blocks,
            "hexagons" => Hexagons,
            "squares" => Squares,
            "circles" => Circles,
            "triangles" => Triangles,
            "parallelograms" => Parallelograms,
            _ => {
                eprintln!("{}: Could not find character set '{}'.", crate::PKG_NAME, s);
                std::process::exit(1);
            }
        }
    }

    pub fn chars(&self) -> (char, char) {
        match self {
            ODot => ('·', 'O'),
            Binary => ('0', '1'),
            Blocks => ('░', '█'),
            Hexagons => ('⬡', '⬢'),
            Squares => ('□', '■'),
            Circles => ('○', '●'),
            Triangles => ('△', '▲'),
            Parallelograms => ('▱', '▰'),
        }
    }

    pub fn charset_list() -> String {
        let mut charset_list = String::new();
        for charset in Self::iter() {
            charset_list.push_str(&format!(
                "{:<16} {} {}\n",
                charset,
                charset.chars().0,
                charset.chars().1,
            ));
        }
        charset_list
    }
}

impl fmt::Display for CharacterSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.pad(&format!("{:?}", self))
    }
}
