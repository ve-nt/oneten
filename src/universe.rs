pub struct Universe {
    space: Vec<Cell>,
    rule: u32,
    char_set: (char, char),
}

impl Universe {
    pub fn new(rule: u32, space: Vec<Cell>, char_set: (char, char)) -> Self {
        Self {
            rule,
            space,
            char_set,
        }
    }

    /// Evolve a universe to the next generation.
    pub fn evolve(&mut self) {
        let mut next_gen = Vec::new();
        for i in 0..self.space.len() {
            next_gen.push(self.next_cell(i));
        }

        self.space = next_gen;
    }

    /// Calculate the next Cell value of index `i`.
    fn next_cell(&self, i: usize) -> Cell {
        if i == 0 || i == &self.space.len() - 1 {
            return Cell::Dead;
        }

        let mut frame: u32 = 0;
        for cell in &[&self.space[i - 1], &self.space[i], &self.space[i + 1]] {
            frame <<= 1;
            frame |= u32::from(*cell);
        }

        Cell::from(self.rule >> frame & 1)
    }
}

impl std::fmt::Display for Universe {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut display = String::new();
        for cell in &self.space {
            display.push(match cell {
                Cell::Dead => self.char_set.0,
                Cell::Alive => self.char_set.1,
            });
        }
        write!(f, "{}", display)
    }
}

#[derive(Debug, PartialEq)]
pub enum Cell {
    Dead,
    Alive,
}

impl From<&Cell> for u32 {
    fn from(source: &Cell) -> Self {
        match source {
            Cell::Dead => 0,
            Cell::Alive => 1,
        }
    }
}

impl From<u32> for Cell {
    fn from(source: u32) -> Self {
        match source {
            1 => Cell::Alive,
            _ => Cell::Dead,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Cell::*;
    use super::*;

    #[test]
    fn test_evolve() {
        let space = vec![
            Alive, Alive, Dead, Alive, Dead, Dead, Dead, Dead, Alive, Dead, Dead, Alive, Dead,
            Dead, Dead, Alive, Dead, Dead, Dead, Dead, Dead, Alive, Dead, Dead, Dead, Dead, Dead,
            Dead, Alive, Alive,
        ];
        let mut gen1 = Universe::new(110, space, ('_', '_'));

        gen1.evolve();

        let gen2 = vec![
            Dead, Alive, Alive, Alive, Dead, Dead, Dead, Alive, Alive, Dead, Alive, Alive, Dead,
            Dead, Alive, Alive, Dead, Dead, Dead, Dead, Alive, Alive, Dead, Dead, Dead, Dead, Dead,
            Alive, Alive, Dead,
        ];

        assert_eq!(gen1.space, gen2);
    }

    #[test]
    fn test_next_cell() {
        let universe = Universe::new(110, vec![Alive, Dead, Dead, Dead, Alive, Alive], ('_', '_'));
        assert_eq!(universe.next_cell(0), Dead);
        assert_eq!(universe.next_cell(1), Dead);
        assert_eq!(universe.next_cell(3), Alive);
        assert_eq!(universe.next_cell(universe.space.len() - 1), Dead);
    }
}
