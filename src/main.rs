mod args;
mod char_set;
mod universe;

const PKG_NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    let args = args::Args::get();

    // Print out available character presets to the user if they've requeset
    // them, then exit gracefully.
    if args.list_charsets {
        print!("{}", char_set::CharacterSet::charset_list());
        std::process::exit(0);
    }

    // Determine the charset to use.
    let char_set = match args.custom_char_set() {
        Some(cs) => cs,
        None => args.char_set(),
    };

    // Generate the universe.
    let mut universe = universe::Universe::new(args.rule, args.universe(), char_set);

    // Evolve the universe, printing out each generation as we go.
    for _ in 0..args.iterations {
        println!("{}", universe);
        universe.evolve();
    }
}
